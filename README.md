# Parasoft Jtest Integration for GitLab

This project provides example pipelines that demonstrate how to integrate Parasoft Jtest with GitLab. The integration enables you to run code analysis with Parasoft Jtest and review analysis results directly in GitLab.

Parasoft Jtest is a testing tool that automates software quality practices for Java applications. It uses a comprehensive set of analysis techniques, including pattern-based static analysis, dataflow analysis, metrics, code coverage, and unit testing to help you verify code quality and ensure compliance with industry standards, such as CWE, OWASP, and CERT.
- Request [a free trial](https://www.parasoft.com/products/parasoft-jtest/jtest-request-a-demo/) to receive access to Parasoft Jtest's features and capabilities.
- See the [user guide](https://docs.parasoft.com/display/JTEST20231) for information about Parasoft Jtest's capabilities and usage.

Please visit the [official Parasoft website](http://www.parasoft.com) for more information about Parasoft Jtest and other Parasoft products.

- [Quick start](#quick-start)
- [Example Pipelines](#example-pipelines)
- [Reviewing Analysis Results](#reviewing-analysis-results)

## Quick start

To analyze your code with Parasoft Jtest and review analysis results in GitLab, you need to customize your pipeline to include a job that will:
* build the tested project with Gradle or Maven.
* run Jtest.
* upload the analysis report in the SAST format.
* upload the Jtest analysis reports in other formats (XML, HTML, etc.).

### Prerequisites

* This extension requires Parasoft Jtest 2021.2 (or newer) with a valid Parasoft license. Starting with GitLab v.16.0.0 and later only SAST report v.15 is accepted. SAST report v.15 is supported in Jtest v.2023.1.1 and later.
* We recommend that you execute the pipeline on a GitLab runner with Parasoft Jtest installed and configured on the runner.

## Example Pipelines
The following examples show simple pipelines for Gradle and Maven projects. The examples assume that Jtest is run on a GitLab runner and the path to the `jtestcli` executable is available on `PATH`.

#### Run Jtest with Gradle project

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/jtest-gitlab/-/blob/master/pipelines/jtest-gradle/.gitlab-ci.yml) file.


```yaml
# This is a basic pipeline to help you get started with Jtest integration to analyze a Gradle project.

stages:        
  - test

# Builds the project with Gradle to run code analysis with Jtest.
build-gradle:
  stage: test
  script:
    # When running on Windows with PowerShell 5.1, be sure to enforce the default file encoding:
      # - $PSDefaultParameterValues['Out-File:Encoding'] = 'default'

    # Configures advanced reporting options and SCM integration.
    - echo "report.format=xml,html,sast-gitlab" > report.properties
    - echo "report.scontrol=min" >> report.properties
    - echo "scontrol.rep.type=git" >> report.properties
    - echo "scontrol.rep.git.url=$CI_PROJECT_URL" >> report.properties
    - echo "scontrol.branch=$CI_COMMIT_BRANCH" >> report.properties
    # When running on Windows, be sure to escape backslashes:
      # - echo "scontrol.rep.git.workspace=$CI_PROJECT_DIR".Replace("\", "\\") >> report.properties
    - echo "scontrol.rep.git.workspace=$CI_PROJECT_DIR" >> report.properties

    # Launches Jtest.
    - echo "Running Jtest..."
    - gradle build -I path/to/jtest/integration/gradle/init.gradle jtest "-Djtest.config=builtin://Recommended Rules" "-Djtest.settings=report.properties" "-Djtest.report=reports"

  artifacts:
    # Uploads analysis results in the GitLab SAST format, so that they are displayed in GitLab.
    reports:
      sast: reports/report.sast
    # Uploads all report files (.xml, .html, .sast) as build artifacts.
    paths:
      - reports/*
```

#### Run Jtest with Maven project

See also the example [.gitlab-ci.yml](https://gitlab.com/parasoft/jtest-gitlab/-/blob/master/pipelines/jtest-maven/.gitlab-ci.yml) file.


```yaml
# This is a basic pipeline to help you get started with Jtest integration to analyze a Maven project.

stages:
  - test

# Builds the project with Maven to run code analysis with Jtest.
build-maven:
  stage: test
  script:
    # When running on Windows with PowerShell 5.1, be sure to enforce the default file encoding:
      # - $PSDefaultParameterValues['Out-File:Encoding'] = 'default'

    # Configures advanced reporting options and SCM integration.
    - echo "report.format=xml,html,sast-gitlab" > report.properties
    - echo "report.scontrol=min" >> report.properties
    - echo "scontrol.rep.type=git" >> report.properties
    - echo "scontrol.rep.git.url=$CI_PROJECT_URL" >> report.properties
    - echo "scontrol.branch=$CI_COMMIT_BRANCH" >> report.properties
    # When running on Windows, be sure to escape backslashes:
      # - echo "scontrol.rep.git.workspace=$CI_PROJECT_DIR".Replace("\", "\\") >> report.properties
    - echo "scontrol.rep.git.workspace=$CI_PROJECT_DIR" >> report.properties

    # Launches Jtest.
    - echo "Running Jtest..."
    - mvn install jtest:jtest "-Djtest.config=builtin://Recommended Rules" "-Djtest.settings=report.properties" "-Djtest.report=reports"

  artifacts:
    # Uploads analysis results in the GitLab SAST format, so that they are displayed in GitLab.
    reports:
      sast: reports/report.sast
    # Uploads all report files (.xml, .html, .sast) as build artifacts.
    paths:
      - reports/*
```

## Reviewing Analysis Results
When the pipeline completes, you can review the violations reported by Jtest as code vulnerabilities:
* in the *Security* tab of the GitLab pipeline.
* on GitLab's Vulnerability Report.

You can click each violation reported by Jtest to review the details and navigate to the code that triggered the violation.

## Baselining Static Analysis Results in Merge Requests
In GitLab, when a merge request is created, static analysis results generated for the branch to be merged are compared with the results generated for the integration branch. As a result, only new violations are presented in the merge request view, allowing developers to focus on the relevant problems for their code changes. 

### Defining a Merge Request Policy
You can define a merge request policy for your integration branch that will block merge requests due to new violations. To configure this:
1. In your GitLab project view, go to Security & Compliance>Policies, and select New policy.
1. Select the Scan result policy type.
1. In the Policy details section, define a rule for Static Application Security Testing (select “IF SAST…”). Configure additional options, if needed.

---
## About
Jtest integration for GitLab - Copyright (C) 2023 Parasoft Corporation
